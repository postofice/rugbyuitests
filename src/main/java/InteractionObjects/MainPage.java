package InteractionObjects;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class MainPage  extends AbstractPage {
    public String url() {
        return Configuration.baseUrl = "http://nzrtv-test.ap-southeast-2.elasticbeanstalk.com/live/home";
    }

    public SelenideElement title = $(By.cssSelector(".navbar-brand"));

    public SelenideElement signBtn = $(By.cssSelector(".sign-button"));



    public String getTitle() {
        return title.getAttribute("innerText");
    }

    public void logOut()
    {
        signBtn.click();
    }
}
