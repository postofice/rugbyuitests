package InteractionObjects;

import com.codeborne.selenide.*;
import org.openqa.selenium.By;
import java.util.List;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;


public class LoginPage extends MainPage {

    public String url() {
        return Configuration.baseUrl = "http://nzrtv-test.ap-southeast-2.elasticbeanstalk.com/live/home";
    }

    public SelenideElement emailInput = $(By.cssSelector("[name='email']"));

    public SelenideElement passInput = $(By.cssSelector("[name='password']"));

    public SelenideElement logInBtn = $(By.cssSelector("button.auth0-lock-submit[type='submit']"));

    public SelenideElement errorMsg = $(By.cssSelector(".auth0-global-message-error"));

    public List<SelenideElement> errorPopups = $$(By.cssSelector(".auth0-lock-error-msg"));

  //  public SelenideElement notYourAccountLink = $(By.linkText("Not your account?"));
    public By notYourAccountLink = By.linkText("Not your account?");

    public By dontRememberYourPassLink = By.linkText("Don't remember your password?");

    private SelenideElement socialButtonsContainer = $(By.cssSelector(".auth0-lock-social-buttons-container"));

    public SelenideElement fbButton = socialButtonsContainer.$(By.cssSelector(".auth0-lock-social-button[data-provider='facebook']"));

    public SelenideElement googleButton = socialButtonsContainer.$(By.cssSelector(".auth0-lock-social-button[data-provider='google-oauth2']"));

    public SelenideElement twitterButton = socialButtonsContainer.$(By.cssSelector(".auth0-lock-social-button[data-provider='twitter']"));

    public void Login(String login, String pass)
    {
        emailInput.sendKeys(login);
        passInput.sendKeys(pass);
        logInBtn.click();
    }

    public AbstractPage OpenPage()
    {
        clearCookie();
        super.OpenPage();

        if (get(dontRememberYourPassLink, 5000) == null)
            get(notYourAccountLink, 5000).click();
        return this;
    }

    public void clearCookie()
    {
        WebDriverRunner.getWebDriver().manage().deleteAllCookies();
    }
}
