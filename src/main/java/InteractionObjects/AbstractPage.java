package InteractionObjects;

import com.codeborne.selenide.*;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class AbstractPage {
    public AbstractPage() {
        //System.setProperty("webdriver.chrome.driver", "D:\\Projects\\Drivers\\chromedriver.exe");
        //Configuration.browser = "chrome";
        //Configuration.timeout = 6000;
    }

    public String url() {
        return "";
    }

    public AbstractPage OpenPage() {
        Selenide.open(url());
        return this;
    }

    public SelenideElement get(By by, int timeout)
    {
        try {
            return $(by).waitUntil(Condition.appear, timeout);
        }
        catch ( Throwable t){
            return null;
        }
    }
}
