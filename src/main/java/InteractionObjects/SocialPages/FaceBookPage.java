package InteractionObjects.SocialPages;

import InteractionObjects.AbstractPage;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class FaceBookPage extends AbstractPage {

    private SelenideElement email = $(By.cssSelector("#email"));
    private SelenideElement pass = $(By.cssSelector("#pass"));
    private SelenideElement loginBtn = $(By.cssSelector("#loginbutton"));

    public void Login(String e_mail, String password)
    {
        email.sendKeys(e_mail);
        pass.sendKeys(password);
        loginBtn.click();

    }
}
