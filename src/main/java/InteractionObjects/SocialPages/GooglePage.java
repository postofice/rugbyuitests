package InteractionObjects.SocialPages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class GooglePage {

    private SelenideElement email = $(By.cssSelector("[name='identifier']"));
    private SelenideElement pass = $(By.cssSelector("[name='password']"));
    private SelenideElement nextBtn = $(By.cssSelector("#identifierNext"));
    private SelenideElement passBtn = $(By.cssSelector("#passwordNext"));

public void Login(String e_mail, String password)
{
    email.sendKeys(e_mail);
    nextBtn.click();
    pass.sendKeys(password);
    passBtn.click();
}

}
