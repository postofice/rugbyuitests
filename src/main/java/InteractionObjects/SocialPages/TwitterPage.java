package InteractionObjects.SocialPages;

import InteractionObjects.AbstractPage;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class TwitterPage  extends AbstractPage {

    private SelenideElement email = $(By.cssSelector("#username_or_email"));
    private SelenideElement pass = $(By.cssSelector("#password"));
    private SelenideElement loginBtn = $(By.cssSelector("#allow"));

    public void Login(String e_mail, String password)
    {
        email.sendKeys(e_mail);
        pass.sendKeys(password);
        loginBtn.click();
    }
}

