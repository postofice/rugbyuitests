package StepDefinitions;

import InteractionObjects.LoginPage;
import InteractionObjects.MainPage;
import InteractionObjects.SocialPages.FaceBookPage;
import InteractionObjects.SocialPages.GooglePage;
import InteractionObjects.SocialPages.TwitterPage;
import Utils.TestSettings;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class SocialLoginTestDefs {
    //@Autowired
    //private World world;
    private LoginPage loginPage = new LoginPage();
    private MainPage mainPage = new MainPage();

    private void LoginViaSocialAccount(TestSettings.AccountTypes accountType)
    {
        TestSettings.Account account = TestSettings.socialAccounts.get(TestSettings.AccountTypes.Google);
        loginPage.OpenPage();
        switch (accountType) {
            case Facebook:
                loginPage.fbButton.click();
                FaceBookPage fbPage = new FaceBookPage();
                fbPage.Login(account.login, account.pass);
                break;
            case Google:
                loginPage.googleButton.click();
                GooglePage googlePage = new GooglePage();
                googlePage.Login(account.login, account.pass);
                break;
            case Twitter:
                loginPage.twitterButton.click();
                TwitterPage twitterPage = new TwitterPage();
                twitterPage.Login(account.login, account.pass);
                break;
        }
        mainPage.logOut();
    }

    @Given("^my Facebook is associated to an registered NZR ID account$")
    public void my_Facebook_is_associated_to_an_registered_NZR_ID_account() throws Throwable {
        LoginViaSocialAccount(TestSettings.AccountTypes.Facebook);
    }

    @Given("^my Google is associated to an registered NZR ID account$")
    public void my_Google_is_associated_to_an_registered_NZR_ID_account() throws Throwable {
        LoginViaSocialAccount(TestSettings.AccountTypes.Google);
    }

    @Given("^my Twitter is associated to an registered NZR ID account$")
    public void my_Twitter_is_associated_to_an_registered_NZR_ID_account() throws Throwable {
        LoginViaSocialAccount(TestSettings.AccountTypes.Twitter);
    }

    @When("^I login using Facebook$")
    public void i_login_using_Facebook() throws Throwable {
        loginPage.fbButton.click();
    }

    @When("^I login using Google$")
    public void i_login_using_Google() throws Throwable {
        loginPage.googleButton.click();
    }

    @When("^I login using Twitter$")
    public void i_login_using_Twitter() throws Throwable {
        loginPage.twitterButton.click();
    }
}
