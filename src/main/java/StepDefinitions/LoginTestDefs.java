package StepDefinitions;

import InteractionObjects.LoginPage;
import InteractionObjects.MainPage;
import InteractionObjects.SocialPages.FaceBookPage;
import InteractionObjects.SocialPages.GooglePage;
import InteractionObjects.SocialPages.TwitterPage;
import Utils.TestSettings;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import cucumber.runtime.model.CucumberExamples;
import cucumber.runtime.model.CucumberScenario;
import org.apache.logging.log4j.core.util.Assert;
import org.openqa.selenium.Cookie;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Console;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

public class LoginTestDefs {

    String currentEmail, currentPass;

    LoginPage loginPage = new LoginPage();
    MainPage mainPage = new MainPage();

    @Given("^I have registered email address \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void i_have_registered_email_address_and_password(String arg1, String arg2) throws Throwable {
        currentEmail = arg1;
        currentPass = arg2;
    }

    @Given("^I am on the NZR ID login page$")
    public void i_am_on_the_NZR_ID_login_page() throws Throwable {
        loginPage.OpenPage();
    }

    @When("^I login$")
    public void i_login() throws Throwable {
        loginPage.Login(currentEmail, currentPass);
    }

    @Then("^I should see an error message \"([^\"]*)\"$")
    public void i_should_see_an_error_message(String arg1) throws Throwable {
        if (!loginPage.errorPopups.isEmpty()){
            for (SelenideElement element : loginPage.errorPopups) {
                if (element.getAttribute("innerText").contains(arg1))
                    return;

            }
            throw new AssertionError("The error message /" + arg1 +"/ does not exist in popup");
        }
        else {
            String errMsg = loginPage.errorMsg.getAttribute("innerText");
            if (!errMsg.equalsIgnoreCase(arg1)){
                throw new AssertionError("The error message does not exist. Expected: "+ arg1+". Showed: " + errMsg);
            }
        }
    }

    @Then("^I should be redirected to the All Blacks Live Streaming portal$")
    public void i_should_be_redirected_to_the_All_Blacks_Live_Streaming_portal() throws Throwable {
        String title = "ALL BLACKS";
        String curTitle = mainPage.getTitle();
        System.out.println("Current title: " + curTitle);

        if (!curTitle.contains(title))
            throw new AssertionError("The title of page is wrong. Expected: \"" + title + "\". Showed: " + curTitle);
    }

    @When("^I goto the NZR ID login page$")
    public void i_goto_the_NZR_ID_login_page() throws Throwable {
        loginPage.OpenPage();
    }
}
