package Utils;

import java.util.HashMap;
import java.util.Map;

public class TestSettings {

    public static class Account {
        public String login;
        public String pass;

        public Account(String login, String pass) {
            this.login = login;
            this.pass = pass;
        }
    }

    public enum  AccountTypes { Facebook, Google, Twitter
    }

    public static Map<AccountTypes, Account> socialAccounts;
    static {
        socialAccounts = new HashMap<>();
        socialAccounts.put(AccountTypes.Facebook, new Account("rugby.testet@gmail.com", "Password!@"));
        socialAccounts.put(AccountTypes.Google, new Account("rugby.testet@gmail.com", "Password!@"));
        socialAccounts.put(AccountTypes.Twitter, new Account("rugby.testet@gmail.com", "Password!@"));
    }
}


