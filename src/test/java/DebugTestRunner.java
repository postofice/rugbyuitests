import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/features",
        glue = "StepDefinitions",
        tags = "@ORPHANd",
        dryRun = false,
        strict = false,
        snippets = SnippetType.UNDERSCORE,
        plugin = {"json:target/cucumber.json"})
//        name = "^Успешное|Успешная.*")

public class DebugTestRunner {
}