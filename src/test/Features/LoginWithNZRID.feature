# new feature
# Tags: optional
    Feature: Login with NZR ID

      In order to grow Team All Blacks membership and better understand viewers,
      as NZR,
      I want prospective viewers to authenticate with their NZR ID during the purchase process


      @ORPHANd
      Scenario: Login with  email address

        Given I have registered email address "john@snow.co.nz" and password "Test123"
        And I am on the NZR ID login page
        When I login
        Then I should be redirected to the All Blacks Live Streaming portal


      @ORPHAN
      Scenario Outline: Invalid login attempt with email address

        Given I have registered email address "<email>" and password "<password>"
        And I am on the NZR ID login page
        When I login
        Then I should see an error message "<error_message>"

        Examples:
         |email           |password      |error_message                                                                         |
         | a@a.com        |              |Can't be blank                                                                        |
         |                |11111         |Can't be blank                                                                        |
         | sansa3@stark.com|some_password|Wrong email or password.                                                              |
        # | sansa          |some_password |Email address seems to be incomplete. Please include an '@' sign in the email address|
        # | sansa@         |some_password |Email address seems to be incomplete. Please enter the part follwing the '@'.        |
        # | sansa@stark.   |some_password |Email address seems to be incomplete. '.' is used at the wrong position.             |
        # | sansa@stark.c  |some_password |Email address seems to be incomplete. '.c' does not seem to be a valid domain.       |


      @ORPHAN
      Scenario Outline: Login with Social Account

        Given my <social_account> is associated to an registered NZR ID account
        When I goto the NZR ID login page
        And I login using <social_account>
        Then I should be redirected to the All Blacks Live Streaming portal

        Examples:
          | social_account|
          | Facebook      |
          | Google        |
          | Twitter       |